package com.company.Array2;
/*
Given an array length 1 or more of ints, return the difference between the
largest and smallest values in the array. Note: the built-in Math.min(v1, v2)
and Math.max(v1, v2) methods return the smaller or larger of two values.

bigDiff([10, 3, 5, 6]) → 7
bigDiff([7, 2, 10, 9]) → 8
bigDiff([2, 10, 7, 2]) → 8
 */
class BigDiff {
    public static void main(String[] args) {
        int[] arr1 = {1,16,12,13,14};
        System.out.println(Integer.toString(getBigDiff(arr1)));
    }

    private static int getBigDiff(int[] nums){
        int min = nums[0];
        int max = nums[0];
        for (int num:nums){
            if (num<min){
                min = num;
            }
            if (num>max){
                max = num;
            }
        }
        return max-min;
    }
}
