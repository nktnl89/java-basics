package com.company.Logic2;

/*

Given 3 int values, a b c, return their sum. However, if one of the values is 13 then
it does not count towards the sum and values to its right do not count.
So for example, if b is 13, then both b and c do not count.

luckySum(1, 2, 3) → 6
luckySum(1, 2, 13) → 3
luckySum(1, 13, 3) → 1
 */

class LuckySum {
    public static void main(String[] args) {
        System.out.println(Integer.toString(getLuckySum(1,13,13)));
    }
    private static int getLuckySum(int a, int b, int c){
        int sum = 0;
        int[] nums = {a,b,c};
        for (int num:nums){
            if (num==13){
                break;
            }
            sum += num;
        }
        return sum;
    }
}
