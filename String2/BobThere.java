package com.company.String2;

/*
Return true if the given string contains a "bob" string, but where the middle 'o' char can be any char.


bobThere("abcbob") → true
bobThere("b9b") → true
bobThere("bac") → false
 */

class BobThere {
    public static void main(String[] args) {
        System.out.println(getBobThere("babab"));
    }
    private static boolean getBobThere(String str){
        int firstB = 0;
        char[] symbArray = new char[str.length()-1];
        symbArray = str.toCharArray();
        for (int i=0;i<symbArray.length;i++){
            if (symbArray[i]=='b'&&(i-firstB==2)){
                return true;
            }
            if (symbArray[i]=='b'){
                firstB = i;
            }
        }
        return false;
    }
}
