package com.company.String2;

/*
Return the number of times that the string "code" appears anywhere in the
given string, except we'll accept any letter for the 'd', so "cope" and "cooe" count.


countCode("aaacodebbb") → 1
countCode("codexxcode") → 2
countCode("cozexxcope") → 2
 */

class CountCode {
    public static void main(String[] args) {
        System.out.println(Integer.toString(getCountCode("codecodecde")));
    }
    private static int getCountCode(String str){
        int fromIndex = 0;
        int countCode = 0;
        while (str.indexOf("code",fromIndex)!=(-1)){
            fromIndex = str.indexOf("code",fromIndex)+4;
            countCode ++;
        }
        return countCode;
    }
}
