package com.company.String3;

/*
Given a string, count the number of words ending in 'y' or 'z' -- so the 'y'
in "heavy" and the 'z' in "fez" count, but not the 'y' in "yellow" (not case sensitive).
We'll say that a y or z is at the end of a word if there is not an alphabetic letter immediately following it.
(Note: Character.isLetter(char) tests if a char is an alphabetic letter.)

countYZ("fez day") → 2
countYZ("day fez") → 2
countYZ("day fyyyz") → 2
 */

class CountYZ {
    public static void main(String[] args) {
        System.out.println(getCountYZ("z"));
    }
    private static Integer getCountYZ(String str){
        Integer countYZ = 0;
        char prevSym = (char) '_';
        for (int i = 0;i<str.length(); i++){
            if (((prevSym == 'z'||prevSym == 'y')&&(!Character.isLetter(str.charAt(i)))||((str.charAt(i)=='y'||str.charAt(i)=='z')&&i==str.length()-1))){
                countYZ++;
            }
            prevSym = str.charAt(i);
        }
        return countYZ;

    }
}
