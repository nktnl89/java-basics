package com.company.Array3;

/*
Given n>=0, create an array length n*n with the following pattern,
shown here for n=3 : {0, 0, 1,    0, 2, 1,    3, 2, 1} (spaces added to show the 3 groups).

squareUp(3) → [0, 0, 1, 0, 2, 1, 3, 2, 1]
squareUp(2) → [0, 1, 2, 1]
squareUp(4) → [0, 0, 0, 1, 0, 0, 2, 1, 0, 3, 2, 1, 4, 3, 2, 1]
 */

class SquareUp {
    public static void main(String[] args) {
        int[] numArray = getSquareUp(3);
        for (int num:numArray){
            System.out.print(Integer.toString(num));
        }
    }
    private static int[] getSquareUp(int n){
        int[] resultArray = new int[n*n];
        for (int i=0;i<resultArray.length;i++){
            resultArray[i] = (int) (Math.random()*n+1);
        }
        return resultArray;
    }
}
